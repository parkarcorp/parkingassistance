package com.parkingassist;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.parkingassist.datamodels.ParkingSlot;
import com.parkingassist.models.PMFHttpServlet;
import com.parkingassist.models.ServerResponse;

@SuppressWarnings("serial")
public class FetchStatusServlet extends PMFHttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		ServerResponse<List<ParkingSlot>> response;

		List<ParkingSlot> savedData = getPersistentObjects(ParkingSlot.class);
		if (savedData == null || savedData.size() == 0) {
			response = new ServerResponse<List<ParkingSlot>>(1, "No Records",
					null);
		} else {
			response = new ServerResponse<List<ParkingSlot>>(0, null,
					savedData);
		}

		resp.getWriter().write(new Gson().toJson(response));
	}
}
