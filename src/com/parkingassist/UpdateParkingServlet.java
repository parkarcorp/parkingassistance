package com.parkingassist;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.parkingassist.datamodels.ParkingSlot;
import com.parkingassist.models.PMFHttpServlet;
import com.parkingassist.models.ServerResponse;
import com.parkingassist.utils.StringUtils;

public class UpdateParkingServlet extends PMFHttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String jsonRequest = getRequestString(req);

		ServerResponse<ParkingSlot> response;
		if (!StringUtils.isNullOrEmpty(jsonRequest)) {
			ParkingSlot requestData = new Gson().fromJson(jsonRequest,
					ParkingSlot.class);
			System.out.println("request : " + requestData);
			try {
				
				ParkingSlot saved = getPersistentObject(ParkingSlot.class, requestData.getParkingId());
				
				if (saved != null) {
					saved.setStatus(requestData.getStatus());
					getLastManager().close();
					response = new ServerResponse<ParkingSlot>(0, "Parking slot updated",
							saved);
				} else {
					response = new ServerResponse<ParkingSlot>(1,
							"No parking found",
							null);
				}
			} catch (Exception e) {
				response = new ServerResponse<ParkingSlot>(1,
						"Failed to update parking slot",
						null);
			}

		} else {
			response = new ServerResponse<ParkingSlot>(1, "Invalid Request",
					null);
		}

		resp.getWriter().write(new Gson().toJson(response));
	}

}