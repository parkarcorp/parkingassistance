package com.parkingassist.models;

/**
 * @author basitparkar
 */
public class ServerResponse<T> {

    int errorCode;
    String errorDesc;
    T data;

    public ServerResponse(int errorCode, String errorDesc, T data) {
        this.errorCode = errorCode;
        this.errorDesc = errorDesc;
        this.data = data;
    }
}
