package com.parkingassist;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.parkingassist.datamodels.ParkingSlot;
import com.parkingassist.models.PMFHttpServlet;
import com.parkingassist.models.ServerResponse;
import com.parkingassist.utils.StringUtils;

public class AddParkingServlet extends PMFHttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String jsonRequest = getRequestString(req);

		ServerResponse<String> response;
		if (!StringUtils.isNullOrEmpty(jsonRequest)) {
			ParkingSlot requestData = new Gson().fromJson(jsonRequest,
					ParkingSlot.class);
			System.out.println("request : " + requestData);
			try {
				if (doPersistent(requestData)) {
					response = new ServerResponse<String>(0, "Parking slot added",
							null);
				} else {
					response = new ServerResponse<String>(1,
							"Failed to add parking slot",
							null);
				}
			} catch (Exception e) {
				response = new ServerResponse<String>(1,
						"Failed to add parking slot",
						null);
			}

		} else {
			response = new ServerResponse<String>(1, "Invalid Request",
					null);
		}

		resp.getWriter().write(new Gson().toJson(response));
	}

}